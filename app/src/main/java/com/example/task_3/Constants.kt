package com.example.task_3

import android.Manifest
import android.content.pm.PackageManager

const val READ_CONTACTS = Manifest.permission.READ_CONTACTS
const val CONTACT_PERMISSION_CODE = 100
const val CONTACT_PICK_CODE = 200
const val READ_PERMISSION = PackageManager.PERMISSION_GRANTED
const val SP_STRING_KEY = "STRING_KEY"
const val SP_NUMBER_NAME = "SharedPreference"
const val CHANNEL_ID = "Contact channel id"
const val NOTIFICATION_ID = 101