package com.example.task_3

import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import com.example.task_3.data.Contact
import com.example.task_3.data.ContactDatabase
import com.example.task_3.data.ContactViewModel
import com.example.task_3.data.MyNumber
import com.example.task_3.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity(), ContactAdapter.RecyclerViewItemClickListener {

    private lateinit var binding: ActivityMainBinding
    private lateinit var contactViewModel: ContactViewModel
    private var customDialog: ContactViewDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        contactViewModel = ViewModelProvider(this).get(ContactViewModel::class.java)

        binding.btSelectContact.setOnClickListener {
            if (checkContactPermission()) {
                pickContact()
            } else {
                requestContactPermission()
            }
        }

        binding.btShowContact.setOnClickListener {
            clickHere()
        }

        binding.btShowSp.setOnClickListener {
            val sharedPreferences = getSharedPreferences(SP_NUMBER_NAME, MODE_PRIVATE)
            val contactSP = sharedPreferences.getString(SP_STRING_KEY, null)
            Snackbar.make(it, "$contactSP", Snackbar.LENGTH_SHORT).show()
        }

        binding.btShowNotification.setOnClickListener {
            createNotificationChannel()
            val sharedPreferences = getSharedPreferences(SP_NUMBER_NAME, MODE_PRIVATE)
            val contactSP = sharedPreferences.getString(SP_STRING_KEY, null)
            if (contactSP != null) {
                getCurrencyContact(contactSP).observe(this, {
                    Notification().sendNotification(
                        this,
                        "${it.name}",
                        "Currency contact",
                        NOTIFICATION_ID
                    )
                })
            }
        }
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Currency contact"
            val descriptionText = "Hello"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun checkContactPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            READ_CONTACTS
        ) == READ_PERMISSION
    }

    private fun pickContact() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
        startActivityForResult(intent, CONTACT_PICK_CODE)
    }

    private fun requestContactPermission() {
        val permission = arrayOf(READ_CONTACTS)
        ActivityCompat.requestPermissions(this, permission, CONTACT_PERMISSION_CODE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CONTACT_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == READ_PERMISSION) {
                pickContact()
            } else {
                Toast.makeText(this, "Разрешение отклонено", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CONTACT_PICK_CODE && resultCode == Activity.RESULT_OK) {
            val contactUri: Uri = data?.data ?: return
            val cols = arrayOf(
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
            )

            val cursor = contentResolver.query(
                contactUri, cols, null, null, null
            )

            if (cursor?.moveToFirst()!!) {
                val number = cursor.getString(0)
                val name = cursor.getString(1)

                val contact = Contact(number, name)
                contactViewModel.addContact(contact)
                Toast.makeText(this, "Сохранение успешно", Toast.LENGTH_SHORT).show()
            }
            cursor.close()
        } else {
            Toast.makeText(this, "Отменен", Toast.LENGTH_SHORT).show()
        }

    }

    private fun clickHere() {
        val liveDataNumber = ContactDatabase.getDatabase(this).contactDao().getAllNumber()
        liveDataNumber.observe(this, {
            val dataAdapter = ContactAdapter(it, this)
            customDialog = ContactViewDialog(this, dataAdapter)
            customDialog?.show()
        })
    }

    override fun clickOnItem(myNumber: MyNumber) {
        val contactNumber = myNumber.number
        binding.tvContactInfo.visibility = View.VISIBLE
        val liveDataNumber =
            getCurrencyContact(contactNumber)
        liveDataNumber.observe(this, {
            (contactNumber + "\n" + it.name).also { binding.tvContactInfo.text = it }
        })

        val sharedPreferences = getSharedPreferences(SP_NUMBER_NAME, MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply {
            putString(SP_STRING_KEY, contactNumber)
        }.apply()

        if (customDialog != null) {
            customDialog!!.dismiss()
        }
    }

    private fun getCurrencyContact(number: String): LiveData<Contact> {
        return ContactDatabase.getDatabase(this).contactDao().getContactByNumber(number)
    }
}