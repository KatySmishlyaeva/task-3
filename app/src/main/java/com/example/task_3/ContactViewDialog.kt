package com.example.task_3

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.view.Window
import androidx.recyclerview.widget.RecyclerView

class ContactViewDialog(activity: Activity, var adapter: RecyclerView.Adapter<*>) :
    Dialog(activity) {

    private var recyclerView: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_layout)

        recyclerView = findViewById(R.id.recycler_view)
        recyclerView?.adapter = adapter
    }
}