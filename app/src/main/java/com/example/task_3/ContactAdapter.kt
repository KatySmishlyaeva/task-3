package com.example.task_3

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.task_3.data.MyNumber

class ContactAdapter(
    private val listNumber: List<MyNumber>,
    internal var recyclerViewItem: RecyclerViewItemClickListener
) : RecyclerView.Adapter<ContactAdapter.ContactViewHolder>() {

    inner class ContactViewHolder(view: View) : RecyclerView.ViewHolder(view),
        View.OnClickListener {
        var mTextView: TextView = view.findViewById(R.id.nameTxt)

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            recyclerViewItem.clickOnItem(listNumber[this.adapterPosition])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_recyclerview, parent, false)
        return ContactViewHolder(view)
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        holder.mTextView.text = listNumber[position].number
    }

    override fun getItemCount(): Int = listNumber.size

    interface RecyclerViewItemClickListener {
        fun clickOnItem(data: MyNumber)
    }
}