package com.example.task_3.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ContactDao {
    @Query("SELECT * FROM contacts_table ORDER BY number ASC")
    fun getAllContacts(): LiveData<List<Contact>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addContact(contact: Contact)

    @Query("SELECT * FROM contacts_table WHERE number = :number")
    fun getContactByNumber(number: String): LiveData<Contact>

    @Query("SELECT number FROM contacts_table")
    fun getAllNumber(): LiveData<List<MyNumber>>
}