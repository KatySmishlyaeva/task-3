package com.example.task_3.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "contacts_table")
data class Contact(
    @PrimaryKey
    val number: String = "",
    val name: String = ""
)