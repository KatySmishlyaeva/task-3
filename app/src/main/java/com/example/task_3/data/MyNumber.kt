package com.example.task_3.data

import androidx.room.ColumnInfo

data class MyNumber(
    @ColumnInfo
    val number: String
)