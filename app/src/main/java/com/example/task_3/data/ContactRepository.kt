package com.example.task_3.data

import androidx.lifecycle.LiveData

class ContactRepository(private val contactDao: ContactDao) {

    val readAllData: LiveData<List<Contact>> = contactDao.getAllContacts()

    fun addContact(contact: Contact) {
        contactDao.addContact(contact)
    }
}